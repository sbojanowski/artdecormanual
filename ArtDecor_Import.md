# Import specyfikacji w �rodowisku ART-DECOR #

## Procedura importu ##

1. Uruchomienie panelu administracyjnego serwera eXist-db w przegladarce internetowej pod adresem `http://<adres-ip>:8877//apps/dashboard/index.html`.
2. Zalogowanie si� przy pomocy has�a administratora poprzez klikni�cie w link w lewym g�rnym rogu aplikacji.
    - User: admin
    - Passowrd: has�o ustalone podczas instalacji serwera eXist-db

3. Uruchomienie aplikacji `Collections`.
4. Wybranie katalogu `apps`., a nast�pnie `decor`.
5. Wybranie katalogu `projects`.
6. Utworzenie nowego katalogu, kt�re nazwa odpowiada prefiksowi, kt�ry zapisany jest w projekcie DECOR.
7. Wgranie pliku specyfikacji w formacie DECOR.

Format nazwy pliku specyfikacji:
```
<prefix>-decor.xml
```

Po wgraniu pliku specyfikacji do w�a�ciwego katalogu na serwerze eXist, specyfikacja b�dzie widoczna w aplikacji ART.